package com.cloudexp;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.amazonaws.regions.Regions;
import com.cloudexp.lambda.AWSLambdaFunctions;
import com.cloudexp.lambda.MergeSortRequest;
import com.cloudexp.tasks.MergeSortOnCloud;
import com.cloudexp.tasks.MergeSortOnDevice;
import com.cloudexp.tasks.OnTaskCompletedListener;

public class MergeSortActivity extends AppCompatActivity implements OnTaskCompletedListener<String> {

    private static final String ACCESS_KEY = "AKIAIZHUTCJPPRLS2MNA";
    private static final String SECRET_KEY = "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";
    private static final int DEFAULT_LENGTH = 10000;

    private TextView txtOutput;
    private AWSLambdaFunctions functions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merge_sort);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        final EditText edtLength = findViewById(R.id.edt_length);
        txtOutput = findViewById(R.id.txt_output);

        initLambda();

        Button btnLocal = findViewById(R.id.btn_local);
        btnLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int length = edtLength.getText().toString().equals("") ? DEFAULT_LENGTH : Integer.parseInt(edtLength.getText().toString());
                new MergeSortOnDevice(MergeSortActivity.this).execute(length);
            }
        });

        Button btnRemote = findViewById(R.id.btn_remote);
        btnRemote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int length = edtLength.getText().toString().equals("") ? DEFAULT_LENGTH : Integer.parseInt(edtLength.getText().toString());
                new MergeSortOnCloud(MergeSortActivity.this, functions).execute(new MergeSortRequest(length));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, LandingActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(String result) {
        txtOutput.setText(result);
    }

    private void initLambda() {
        AWSCredentialsProvider awsCredentialsProvider = new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
            }

            @Override
            public void refresh() {
            }
        };

        LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getApplicationContext(),
                Regions.EU_WEST_2,
                awsCredentialsProvider);

        functions = factory.build(AWSLambdaFunctions.class);
    }

}
