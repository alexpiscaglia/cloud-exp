package com.cloudexp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.amazonaws.amplify.generated.graphql.ListNotesQuery;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;

public class LandingActivity extends AppCompatActivity implements NoteListFragment.OnNoteListFragmentInteractionListener, FileListFragment.OnFileListFragmentInteractionListener {

    private static final String LOG_TAG = "CloudExp";
    private static final int REQ_OPEN_DOCUMENT = 123;
    private static final int REQ_EXTERNAL_STORAGE = 456;
    private static final int REQ_INSERT_NOTE = 789;
    private static final String[] STORAGE_PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private ViewPager viewPager;
    private long startTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        verifyStoragePermissions();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager = findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (viewPager.getCurrentItem()) {
                    case 0:
                        startActivityForResult(new Intent(LandingActivity.this, NoteInsertActivity.class), REQ_INSERT_NOTE);
                        break;
                    case 1:
                        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

                        if (isConnected) {
                            performFileSearch();
                        } else {
                            Snackbar.make(findViewById(R.id.main_content), R.string.msg_offline, Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    default:
                        Snackbar.make(view, R.string.err_default, Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_landing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_merge_sort:
                startActivity(new Intent(this, MergeSortActivity.class));
                return true;
            case R.id.action_logout:
                AWSMobileClient.getInstance().signOut();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == REQ_OPEN_DOCUMENT && resultCode == Activity.RESULT_OK) {
            assert resultData != null;
            Uri uri = resultData.getData();
            assert uri != null;
            final File file = new File(getPath(uri));

            new AlertDialog.Builder(this)
                    .setTitle(R.string.alert_title_warning)
                    .setMessage(R.string.msg_upload_file_confirmation)
                    .setCancelable(false)
                    .setNegativeButton(R.string.alert_btn_negative_text, null)
                    .setPositiveButton(R.string.alert_btn_positive_text, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startTime = System.currentTimeMillis();
                            uploadFile(file);
                        }
                    }).show();
        }

        if (requestCode == REQ_INSERT_NOTE && resultCode == Activity.RESULT_OK) {
            Log.d(LOG_TAG, "Note inserted");
        }
    }

    @Override
    public void onNoteListFragmentInteraction(ListNotesQuery.Item note) {
    }

    @Override
    public void onFileListFragmentInteraction(String fileName) {
    }

    private void verifyStoragePermissions() {
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, STORAGE_PERMISSIONS, REQ_EXTERNAL_STORAGE);
        }
    }

    private void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, REQ_OPEN_DOCUMENT);
    }

    private String getPath(Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            try {
                Cursor cursor = getBaseContext().getContentResolver().query(uri, projection, null, null, null);
                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
                cursor.close();
            } catch (Exception ex) {
                Log.e(LOG_TAG, "Error in getPath", ex);
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    private void uploadFile(File file) {
        TransferUtility transferUtility = TransferUtility.builder()
                .context(getApplicationContext())
                .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                .s3Client(new AmazonS3Client(AWSMobileClient.getInstance()))
                .build();

        TransferObserver uploadObserver = transferUtility.upload(
                "private/" + AWSMobileClient.getInstance().getIdentityId() + "/" + file.getName(),
                file);

        uploadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    long elapsedTime = System.currentTimeMillis() - startTime;
                    Log.d(LOG_TAG, "Completed. Elapsed time in ms: " + elapsedTime);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int)percentDonef;

                Log.d(LOG_TAG, "ID:" + id + " bytesCurrent: " + bytesCurrent + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e(LOG_TAG, "Error in uploadFile", ex);
            }
        });

        Log.d(LOG_TAG, "Bytes Transferred: " + uploadObserver.getBytesTransferred());
        Log.d(LOG_TAG, "Bytes Total: " + uploadObserver.getBytesTotal());
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return NoteListFragment.newInstance(1);
                case 1:
                    return FileListFragment.newInstance(1);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

    }

}
