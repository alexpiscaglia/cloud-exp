package com.cloudexp.lambda;

import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction;

public interface AWSLambdaFunctions {

    @LambdaFunction
    MergeSortResponse mergeSort(MergeSortRequest request);

}
