package com.cloudexp.lambda;

public class MergeSortResponse {

    private final int executionTime;

    public MergeSortResponse(final int executionTime) {
        this.executionTime = executionTime;
    }

    public int getExecutionTime() {
        return executionTime;
    }

}
