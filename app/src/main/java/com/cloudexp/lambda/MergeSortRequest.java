package com.cloudexp.lambda;

public class MergeSortRequest {

    private final int length;

    public MergeSortRequest(final int length) {
        this.length = length;
    }

    public int getLength() {
        return this.length;
    }

}
