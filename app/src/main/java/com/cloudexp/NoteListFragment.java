package com.cloudexp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amazonaws.amplify.generated.graphql.DeleteNoteMutation;
import com.amazonaws.amplify.generated.graphql.ListNotesQuery;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.cloudexp.database.DatabaseManager;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import static com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread;

public class NoteListFragment extends Fragment {

    private static final String LOG_TAG = "CloudExp";
    private static final String COLUMN_COUNT = "column_count";

    private int columnCount = 1;
    private OnNoteListFragmentInteractionListener listener;
    private DatabaseManager dbManager;
    private List<ListNotesQuery.Item> notes;
    private NoteRecyclerViewAdapter adapter;
    private long startTime;

    private GraphQLCall.Callback<ListNotesQuery.Data> listNotesCallback = new GraphQLCall.Callback<ListNotesQuery.Data>() {
        @Override
        public void onResponse(@Nonnull final Response<ListNotesQuery.Data> response) {
            long elapsedTime = System.currentTimeMillis() - startTime;
            Log.d(LOG_TAG, "Elapsed time in ms: " + elapsedTime);
            assert response.data() != null;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notes.addAll(response.data().listNotes().items());
                    adapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onFailure(@Nonnull ApolloException ex) {
            Log.e(LOG_TAG, "Error in listNotesCallback", ex);
        }
    };

    private GraphQLCall.Callback<DeleteNoteMutation.Data> deleteNoteCallback = new GraphQLCall.Callback<DeleteNoteMutation.Data>() {
        @Override
        public void onResponse(@Nonnull Response<DeleteNoteMutation.Data> response) {
            long elapsedTime = System.currentTimeMillis() - startTime;
            Log.d(LOG_TAG, "Elapsed time in ms: " + elapsedTime);
            //dbManager.listNotes(listNotesCallback);
        }

        @Override
        public void onFailure(@Nonnull ApolloException ex) {
            Log.e(LOG_TAG, "Error in deleteNoteCallback", ex);
        }
    };

    public NoteListFragment() {
    }

    public static NoteListFragment newInstance(int columnCount) {
        NoteListFragment fragment = new NoteListFragment();
        Bundle args = new Bundle();
        args.putInt(COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNoteListFragmentInteractionListener) {
            listener = (OnNoteListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnNoteListFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            columnCount = getArguments().getInt(COLUMN_COUNT);
        }

        dbManager = new DatabaseManager(getContext());
        notes = new ArrayList<>();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.note_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (columnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, columnCount));
            }
            adapter = new NoteRecyclerViewAdapter(notes);
            recyclerView.setAdapter(adapter);
            assert getContext() != null;
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        }

        startTime = System.currentTimeMillis();
        dbManager.listNotes(listNotesCallback);
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnNoteListFragmentInteractionListener {
        void onNoteListFragmentInteraction(ListNotesQuery.Item note);
    }

    public class NoteRecyclerViewAdapter extends RecyclerView.Adapter<NoteRecyclerViewAdapter.ViewHolder> {

        private final List<ListNotesQuery.Item> notes;

        private final View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListNotesQuery.Item note = (ListNotesQuery.Item) view.getTag();

                Context context = view.getContext();
                Intent intent = new Intent(context, NoteDetailActivity.class);
                intent.putExtra(NoteDetailFragment.NOTE_TITLE, note.title());
                intent.putExtra(NoteDetailFragment.NOTE_CONTENT, note.content());
                context.startActivity(intent);
            }
        };

        private final View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final ListNotesQuery.Item note = (ListNotesQuery.Item) view.getTag();

                assert getContext() != null;
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.alert_title_warning)
                        .setMessage(R.string.msg_delete_note_confirmation)
                        .setCancelable(false)
                        .setNegativeButton(R.string.alert_btn_negative_text, null)
                        .setPositiveButton(R.string.alert_btn_positive_text, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startTime = System.currentTimeMillis();
                                dbManager.deleteNote(note.id(), deleteNoteCallback);
                            }
                        }).show();

                return false;
            }
        };

        NoteRecyclerViewAdapter(List<ListNotesQuery.Item> notes) {
            this.notes = notes;
        }

        @Override
        @NonNull
        public NoteRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_list_content, parent, false);
            return new NoteRecyclerViewAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final NoteRecyclerViewAdapter.ViewHolder holder, int position) {
            holder.txtTitle.setText(notes.get(position).title());
            holder.txtLastUpdate.setText(notes.get(position).lastUpdate());
            holder.txtContent.setText(notes.get(position).content());

            holder.itemView.setTag(notes.get(position));
            holder.itemView.setOnClickListener(onClickListener);
            holder.itemView.setOnLongClickListener(onLongClickListener);
        }

        @Override
        public int getItemCount() {
            return notes.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private final TextView txtTitle;
            private final TextView txtLastUpdate;
            private final TextView txtContent;

            ViewHolder(View view) {
                super(view);
                txtTitle = view.findViewById(R.id.txt_title);
                txtLastUpdate = view.findViewById(R.id.txt_last_update);
                txtContent = view.findViewById(R.id.txt_content);
            }

        }

    }

}
