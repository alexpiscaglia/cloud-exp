package com.cloudexp;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FileListFragment extends Fragment {

    private static final String LOG_TAG = "CloudExp";
    private static final String COLUMN_COUNT = "column_count";

    private int columnCount = 1;
    private OnFileListFragmentInteractionListener mListener;
    private List<Trio<String, String, String>> info;
    private FileRecyclerViewAdapter adapter;
    private long startTime;

    public FileListFragment() {
    }

    public static FileListFragment newInstance(int columnCount) {
        FileListFragment fragment = new FileListFragment();
        Bundle args = new Bundle();
        args.putInt(COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFileListFragmentInteractionListener) {
            mListener = (OnFileListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFileListFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            columnCount = getArguments().getInt(COLUMN_COUNT);
        }

        info = new ArrayList<>();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.file_list, container, false);

        assert getContext() != null;

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (columnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, columnCount));
            }
            adapter = new FileRecyclerViewAdapter(info);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        }

        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (isConnected) {
            new LoadFileList().execute("cloudexp-bucket", "private/" + AWSMobileClient.getInstance().getIdentityId());
        }

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void downloadFile(final File file) {
        assert getActivity() != null;

        TransferUtility transferUtility = TransferUtility.builder()
                .context(getActivity())
                .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                .s3Client(new AmazonS3Client(AWSMobileClient.getInstance()))
                .build();

        TransferObserver downloadObserver = transferUtility.download(
                "private/" + AWSMobileClient.getInstance().getIdentityId() + "/" + file.getName(),
                file);

        downloadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    long elapsedTime = System.currentTimeMillis() - startTime;
                    Log.d(LOG_TAG, "Completed. Elapsed time in ms: " + elapsedTime);
                    openFile(file);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float)bytesCurrent/(float)bytesTotal) * 100;
                int percentDone = (int)percentDonef;

                Log.d(LOG_TAG, "   ID:" + id + "   bytesCurrent: " + bytesCurrent + "   bytesTotal: " + bytesTotal + " " + percentDone + "%");
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e(LOG_TAG, "Error in downloadFIle", ex);
            }
        });

        Log.d(LOG_TAG, "Bytes Transferred: " + downloadObserver.getBytesTransferred());
        Log.d(LOG_TAG, "Bytes Total: " + downloadObserver.getBytesTotal());
    }

    private void openFile(File file) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(file), getMimeType(file.getAbsolutePath()));
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Log.e(LOG_TAG, "Error in openFile", ex);
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public interface OnFileListFragmentInteractionListener {
        void onFileListFragmentInteraction(String fileName);
    }

    public class FileRecyclerViewAdapter extends RecyclerView.Adapter<FileRecyclerViewAdapter.ViewHolder> {

        private final List<Trio<String, String, String>> info;

        private final View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fileName = (String) view.getTag();
                File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                File file = new File(dir, fileName);

                if (file.exists()) {
                    openFile(file);
                } else {
                    startTime = System.currentTimeMillis();
                    downloadFile(file);
                }
            }
        };

        FileRecyclerViewAdapter(List<Trio<String, String, String>> info) {
            this.info = info;
        }

        @Override
        @NonNull
        public FileRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_list_content, parent, false);
            return new FileRecyclerViewAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final FileRecyclerViewAdapter.ViewHolder holder, int position) {
            holder.txtFileName.setText(info.get(position).getFirst());
            holder.txtSize.setText(info.get(position).getSecond());
            holder.txtLastModified.setText(info.get(position).getThird());

            holder.itemView.setTag(info.get(position).getFirst());
            holder.itemView.setOnClickListener(onClickListener);
        }

        @Override
        public int getItemCount() {
            return info.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private final TextView txtFileName;
            private final TextView txtSize;
            private final TextView txtLastModified;

            ViewHolder(View view) {
                super(view);
                txtFileName = view.findViewById(R.id.txt_file_name);
                txtSize = view.findViewById(R.id.txt_size);
                txtLastModified = view.findViewById(R.id.txt_last_modified);
            }

        }

    }

    @SuppressLint("StaticFieldLeak")
    private class LoadFileList extends AsyncTask<String, Void, List<Trio<String, String, String>>> {

        private static final String ACCESS_KEY = "AKIAIZHUTCJPPRLS2MNA";
        private static final String SECRET_KEY = "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";

        @Override
        protected List<Trio<String, String, String>> doInBackground(String... params) {
            AWSCredentialsProvider awsCredentialsProvider = new AWSCredentialsProvider() {
                @Override
                public AWSCredentials getCredentials() {
                    return new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
                }

                @Override
                public void refresh() {
                }
            };

            AmazonS3Client s3Client = new AmazonS3Client(awsCredentialsProvider);
            ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(params[0]).withPrefix(params[1] + "/");

            ObjectListing objects = s3Client.listObjects(listObjectsRequest);
            List<Trio<String, String, String>> info = new ArrayList<>();

            while (true) {
                List<S3ObjectSummary> summaries = objects.getObjectSummaries();
                if (summaries.size() < 1) {
                    break;
                }
                for (S3ObjectSummary summary : summaries) {
                    int index = summary.getKey().lastIndexOf("/") + 1;
                    String fileName = summary.getKey().substring(index);
                    String lastModified = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ITALY).format(summary.getLastModified());

                    info.add(new Trio<>(fileName, adjustFileSize(summary.getSize()), lastModified));
                }
                objects = s3Client.listNextBatchOfObjects(objects);
            }

            return info;
        }

        @Override
        protected void onPostExecute(List<Trio<String, String, String>> result) {
            info.addAll(result);
            adapter.notifyDataSetChanged();
        }

        private String adjustFileSize(long size) {
            if (size <= 0) {
                return "0";
            }
            final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
            int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
            return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
        }

    }

}
