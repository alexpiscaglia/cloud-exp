package com.cloudexp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NoteDetailFragment extends Fragment {

    public static final String NOTE_TITLE = "note_title";
    public static final String NOTE_CONTENT = "note_content";

    private String title;
    private String content;

    public NoteDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        assert getArguments() != null;
        if (getArguments().containsKey(NOTE_TITLE) && getArguments().containsKey(NOTE_CONTENT)) {
            title = getArguments().getString(NOTE_TITLE);
            content = getArguments().getString(NOTE_CONTENT);

            Activity activity = this.getActivity();
            assert activity != null;
            CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(title);
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.note_detail, container, false);
        if (title != null && content != null) {
            ((TextView) rootView.findViewById(R.id.txt_title_detail)).setText(title);
            ((TextView) rootView.findViewById(R.id.txt_content_detail)).setText(content);
        }
        return rootView;
    }

}
