package com.cloudexp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.SignInUIOptions;
import com.amazonaws.mobile.client.UserStateDetails;

public class LoginActivity extends AppCompatActivity {

    private static final String LOG_TAG = "CloudExp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initClient();
    }

    private void initClient() {
        AWSMobileClient.getInstance().initialize(getApplicationContext(), new Callback<UserStateDetails>() {
            @Override
            public void onResult(UserStateDetails userStateDetails) {
                switch (userStateDetails.getUserState()){
                    case SIGNED_IN:
                        startActivity(new Intent(LoginActivity.this, LandingActivity.class));
                        finish();
                        break;
                    case SIGNED_OUT:
                        showSignIn();
                        break;
                    default:
                        AWSMobileClient.getInstance().signOut();
                        break;
                }
            }

            @Override
            public void onError(Exception ex) {
                Log.e(LOG_TAG, "Error in initClient", ex);
            }
        });
    }

    private void showSignIn() {
        AWSMobileClient.getInstance().showSignIn(
                this,
                SignInUIOptions.builder()
                        .nextActivity(LandingActivity.class)
                        .build(),
                new Callback<UserStateDetails>() {
                    @Override
                    public void onResult(UserStateDetails result) {
                        switch (result.getUserState()){
                            case SIGNED_IN:
                                Log.d(LOG_TAG, "Logged in!");
                                break;
                            case SIGNED_OUT:
                                Log.d(LOG_TAG, "User did not choose to sign-in");
                                break;
                            default:
                                AWSMobileClient.getInstance().signOut();
                                break;
                        }
                    }

                    @Override
                    public void onError(Exception ex) {
                        Log.e(LOG_TAG, "Error in showSignIn", ex);
                    }
                }
        );
    }

}
