package com.cloudexp.database;

import android.content.Context;

import com.amazonaws.amplify.generated.graphql.CreateNoteMutation;
import com.amazonaws.amplify.generated.graphql.DeleteNoteMutation;
import com.amazonaws.amplify.generated.graphql.GetNoteQuery;
import com.amazonaws.amplify.generated.graphql.ListNotesQuery;
import com.amazonaws.amplify.generated.graphql.UpdateNoteMutation;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.apollographql.apollo.GraphQLCall;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import type.CreateNoteInput;
import type.DeleteNoteInput;
import type.ModelNoteFilterInput;
import type.ModelStringFilterInput;
import type.UpdateNoteInput;

public class DatabaseManager {

    private AWSAppSyncClient awsAppSyncClient;

    public DatabaseManager(Context context) {
        awsAppSyncClient = AWSAppSyncClient.builder()
                .context(context)
                .awsConfiguration(new AWSConfiguration(context))
                .build();
    }

    public void insertNote(String title, String content, GraphQLCall.Callback<CreateNoteMutation.Data> callback) {
        CreateNoteInput createNoteInput = CreateNoteInput.builder()
                .title(title)
                .content(content)
                .lastUpdate(new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ITALY).format(Calendar.getInstance().getTime()))
                .userIdentityId(AWSMobileClient.getInstance().getIdentityId())
                .build();

        awsAppSyncClient.mutate(CreateNoteMutation.builder().input(createNoteInput).build())
                .enqueue(callback);
    }

    public void updateNote(String id, String newTitle, String newContent, GraphQLCall.Callback<UpdateNoteMutation.Data> callback) {
        UpdateNoteInput updateNoteInput = UpdateNoteInput.builder()
                .id(id)
                .title(newTitle)
                .content(newContent)
                .lastUpdate(new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ITALY).format(Calendar.getInstance().getTime()))
                .build();

        awsAppSyncClient.mutate(UpdateNoteMutation.builder().input(updateNoteInput).build())
                .enqueue(callback);
    }

    public void deleteNote(String id, GraphQLCall.Callback<DeleteNoteMutation.Data> callback) {
        DeleteNoteInput deleteNoteInput = DeleteNoteInput.builder().id(id).build();

        awsAppSyncClient.mutate(DeleteNoteMutation.builder().input(deleteNoteInput).build())
                .enqueue(callback);
    }

    public void listNotes(GraphQLCall.Callback<ListNotesQuery.Data> callback) {
        ModelStringFilterInput stringFilterInput = ModelStringFilterInput.builder()
                .eq(AWSMobileClient.getInstance().getIdentityId())
                .build();

        ModelNoteFilterInput noteFilterInput = ModelNoteFilterInput.builder()
                .userIdentityId(stringFilterInput)
                .build();

        awsAppSyncClient.query(ListNotesQuery.builder().filter(noteFilterInput).build())
                .responseFetcher(AppSyncResponseFetchers.CACHE_FIRST)
                .enqueue(callback);
    }

    public void getNoteById(String id, GraphQLCall.Callback<GetNoteQuery.Data> callback) {
        awsAppSyncClient.query(GetNoteQuery.builder().id(id).build())
                .responseFetcher(AppSyncResponseFetchers.CACHE_FIRST)
                .enqueue(callback);
    }

}
