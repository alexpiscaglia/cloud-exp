package com.cloudexp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.amazonaws.amplify.generated.graphql.CreateNoteMutation;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.cloudexp.database.DatabaseManager;

import javax.annotation.Nonnull;

public class NoteInsertActivity extends AppCompatActivity {

    private static final String LOG_TAG = "CloudExp";

    private long startTime;

    private GraphQLCall.Callback<CreateNoteMutation.Data> insertNoteCallback = new GraphQLCall.Callback<CreateNoteMutation.Data>() {
        @Override
        public void onResponse(@Nonnull Response<CreateNoteMutation.Data> response) {
            long elapsedTime = System.currentTimeMillis() - startTime;
            Log.d(LOG_TAG, "Elapsed time in ms: " + elapsedTime);
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_insert);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        final DatabaseManager dbManager = new DatabaseManager(getApplicationContext());

        final EditText edtTitle = findViewById(R.id.edt_title);
        final EditText edtContent = findViewById(R.id.edt_content);

        final Button btnInsert = findViewById(R.id.btn_insert);
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTime = System.currentTimeMillis();
                dbManager.insertNote(edtTitle.getText().toString(), edtContent.getText().toString(), insertNoteCallback);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, LandingActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
