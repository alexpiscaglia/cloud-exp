package com.cloudexp.tasks;

import android.os.AsyncTask;

import java.util.Random;

public class MergeSortOnDevice extends AsyncTask<Integer, Void, Integer> {

    private final OnTaskCompletedListener<String> listener;

    public MergeSortOnDevice(OnTaskCompletedListener<String> listener) {
        this.listener = listener;
    }

    @Override
    protected Integer doInBackground(Integer... params) {
        int[] numbers = generateRandomIntArray(params[0]);

        long startTime = System.currentTimeMillis();
        mergeSort(numbers);
        long stopTime = System.currentTimeMillis();

        return (int) (stopTime - startTime);
    }

    @Override
    protected void onPostExecute(Integer elapsedTime) {
        listener.onTaskCompleted("Elapsed time in ms: " + elapsedTime);
    }

    private int[] generateRandomIntArray(int length) {
        int[] array = new int[length];
        Random random = new Random();

        for (int i = 0; i < length; i++) {
            array[i] = random.nextInt(10);
        }

        return array;
    }

    private int[] mergeSort (int[] array) {
        int[] tmp = array.clone();
        sort(array, tmp, 0, array.length - 1);
        return array;
    }

    private void sort(int[] array, int[] tmp, int low, int high) {
        if (high <= low) return;

        int mid = low + (high - low) / 2;
        sort(array, tmp, low, mid);
        sort(array, tmp, mid + 1, high);

        merge(array, tmp, low, mid, high);
    }

    private void merge(int[] array, int[] tmp, int low, int middle, int high) {
        for (int k = low; k <= high; k++) {
            tmp[k] = array[k];
        }

        int i = low;
        int j = middle + 1;

        for (int k = low; k <= high; k++) {
            if (i > middle) {
                array[k] = tmp[j++];
            } else if (j > high) {
                array[k] = tmp[i++];
            } else if (tmp[i] < tmp[j]) {
                array[k] = tmp[i++];
            } else {
                array[k] = tmp[j++];
            }
        }
    }

}
