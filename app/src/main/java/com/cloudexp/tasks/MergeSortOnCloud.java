package com.cloudexp.tasks;

import android.os.AsyncTask;

import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunctionException;
import com.cloudexp.lambda.AWSLambdaFunctions;
import com.cloudexp.lambda.MergeSortRequest;
import com.cloudexp.lambda.MergeSortResponse;

public class MergeSortOnCloud extends AsyncTask<MergeSortRequest, Void, MergeSortResponse> {

    private final OnTaskCompletedListener<String> listener;
    private final AWSLambdaFunctions functions;

    public MergeSortOnCloud(OnTaskCompletedListener<String> listener, AWSLambdaFunctions functions) {
        this.listener = listener;
        this.functions = functions;
    }

    @Override
    protected MergeSortResponse doInBackground(MergeSortRequest... params) {
        try {
            return functions.mergeSort(params[0]);
        } catch (LambdaFunctionException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(MergeSortResponse response) {
        if (response == null) {
            listener.onTaskCompleted("Failed to invoke AWS Lambda function");
        } else {
            listener.onTaskCompleted("Elapsed time in ms: " + response.getExecutionTime());
        }
    }

}
