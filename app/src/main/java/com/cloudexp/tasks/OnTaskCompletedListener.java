package com.cloudexp.tasks;

public interface OnTaskCompletedListener<T> {

    void onTaskCompleted(T result);

}
